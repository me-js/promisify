(async () => {
    const script = (pass, onHashed) => {
        console.log('hashing....');
        onHashed(`${pass}-hashed`)
    };

    const promisify = (func) => (pass) => new Promise((res) => {
        func(pass, (data) => res(data))
    })

    // script('test', (hashed) => console.log(hashed))
    const pScript = promisify(script);
    const hashed = await pScript('hello');
    console.log('hashed: ', hashed)
})()