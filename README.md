# Asynchronous Script with Promisify

This is a JavaScript example that demonstrates how to use the `promisify` pattern to convert a callback-based function into a promise-based function. It simplifies asynchronous operations and makes your code more readable.

## Overview

The script provided in this example showcases two main functions:

1. `script(pass, onHashed)`: This function takes a `pass` string and a callback function `onHashed`. It simulates a hashing operation and then invokes the `onHashed` callback with the hashed value.

2. `promisify(func)`: This utility function takes another function `func` as an argument and returns a new function that returns a Promise. It allows you to easily transform callback-based functions into Promise-based functions.

## Usage

To use this script, follow these steps:

1. Clone or download this repository to your local machine.

2. Open the JavaScript file that contains the script.

3. Use the `script` function or the promisified `pScript` function as follows:

   ```javascript
   const script = require('./script.js'); // Replace with the correct path to your script file.

   // Using the script function
   script('test', (hashed) => console.log(hashed));

   // Using the promisified pScript function
   const pScript = script.promisify(script);
   pScript('hello')
     .then((hashed) => console.log('hashed: ', hashed))
     .catch((error) => console.error('Error:', error));
